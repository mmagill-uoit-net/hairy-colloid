import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('WXAgg')
import matplotlib.pyplot as plt

import espressomd as esp
from espressomd import visualization
from espressomd.visualizationMayavi import mlab
from espressomd import lb
#from espressomd import thermostat

import time

        
### Class to store system parameters
class ProblemDefinition:

    ###################################
    ### Parameters have default values, or can be overridden
    def __init__(self,
                 M=6,
                 LP=1000.0,
                 N=5,
                 sig_cor=5.0,
                 outflag=0,
                 visflag=0):

        # Input physical parameters
        self.M = int(M)         # Number of chains
        self.N = int(N)         # Number of particles per chain
        self.LP = float(LP)     # Persistence length
        self.sig_cor = float(sig_cor) # Core diameter relative to hair particles

        # Constant physical parameters
        self.kT = 1.0           # Thermal energy
        self.eps = 1.0          # WCA energy
        self.sig_chn = 1.0      # Size of chain monomers

        # Input running parameters
        self.outflag = int(outflag) # Whether or not to output
        self.visflag = int(visflag) # Whether or not to visualize

        # Constant running parameters
        self.rseed = 1          # Random seed
        self.boxlength = 50     # Cubic box side length
        self.dt = 0.01          # Timestep size
        self.t_eql = 100        # Time to equilibrate 
        self.t_sim = 100        # Time to run main simulation
        
        # Output parameters
        self.outsuffix = '%d_%.1f_%d_%d'%(self.M,self.LP,self.N,int(self.sig_cor))
        # self.df = pd.DataFrame(index=np.arange(self.Nt_sim/self.Nt_cbk)+self.dt*(self.Nt_eql1*10+self.Nt_eql2),
        #                        columns=['COMx','COMy','COMz','<1/rij>'],
        #                        dtype=np.float64)
        # self.df.index.name = 't'
        
        
    ###################################
    ### Clean-up function
    def cleanup(self):
        if self.outflag==1:
            os.system("mv output/*_%s.* outputbak/."%self.outsuffix)

        
    ###################################
    ### Prepare the HOOMD simulation
    def setup(self):
        # Initialize system
        self.system = esp.System()
        self.system.seed = self.rseed
        self.system.cell_system.skin = 0.4
        self.system.time_step = self.dt
        self.system.box_l = [self.boxlength]*3
        # Add the core
        self.system.part.add(pos=[0,0,0], id=0, type=0)
        ## Add the hairs
        #self.system.part.add(pos=[1,0,0], id=i+1, type=1)
        # Lattice-Boltzmann Fluid
        self.lbf = esp.lb.LBFluid_GPU(agrid=1, couple='3pt', dens=1, fric=1, tau=0.01, visc=1)
        self.system.actors.add(self.lbf)
        self.system.thermostat.set_lb(kT=self.kT)
        # Online visualization
        if self.visflag==1:
            self.visualizer = visualization.mayaviLive(self.system)
        # Message
        print("Initialization complete.")

        
    ###################################
    ### Run equilibration
    def eql(self):
        for i in range(self.t_eql):
            self.system.integrator.run(int(1.0/self.dt))
            if self.visflag==1:
                self.visualizer.update()
                self.visualizer.processGuiEvents()
        print("Equilibration stage 1 complete.")


        
    ###################################
    ### Run main simulation
    def run(self):
        for i in range(self.t_eql):
            self.system.integrator.run(int(1.0/self.dt))
            if self.visflag==1:
                self.visualizer.update()
                self.visualizer.processGuiEvents()
        # Keep the visualizer running at the end of the simulation
        if self.visflag==1:
            self.visualizer.start()
        # Message
        print("Simulation complete.")


    ###################################
    ### Integrate for t_int steps
    def integrate(self,t_int):
        print("\tIntegrating for %d units of time."%t_int)
        print("\tFinished.")
