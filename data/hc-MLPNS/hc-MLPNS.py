import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('WXAgg')
import matplotlib.pyplot as plt

import espressomd as esp
from espressomd import visualization
from espressomd.visualizationMayavi import mlab

import hclib_MLPNS as hc


# This is the running interface for the hairy colloid project in Espresso
# The hclib file has a class containing the bulk of the code
# The idea is to calculate the hydrodynamic radii of colloids for all of the following

M = 6       # M:  number of hairs, = 2,3,4,6,8,12,20
LP = 1000.0 # LP: persistence length for every three consecutive monomers
N = 5       # N:  hair length
preS = 2    # S:  core diameter (sig_cor in the code), = 2k+1
S = 2*preS+1                    # For symmetry to sig_cor=1 case


# Run
my = hc.ProblemDefinition()     # Define the system
my.cleanup()                    # Cleanup
my.setup()                      # Initialize
my.eql()                        # Equilibrate
my.run()                        # Run

